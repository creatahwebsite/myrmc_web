 <?php include 'sendemail.php'; ?>
<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noarchive">
    <title> MY RMC </title>
    <!-- favicon -->
    <link rel="shortcut icon" href="fav.png" type="image/x-icon">
    <!-- bootstrap -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- icofont -->
    <link rel="stylesheet" href="assets/css/fontawesome.5.7.2.css">
    <!-- flaticon -->
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <!-- animate.css -->
    <link rel="stylesheet" href="assets/css/animate.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <!-- stylesheet -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- responsive -->
    <link rel="stylesheet" href="assets/css/responsive.css">
    <link rel="stylesheet" href="assets/css/videostyle.css">
</head>

<body>
     
    
<!-- navbar area start -->
    <nav class="navbar navbar-area navbar-expand-lg nav-absolute white nav-style-01 header-style-09">
        <div class="container nav-container">
            <div class="responsive-mobile-menu">
                <div class="logo-wrapper">
                    <a href="index.html" class="logo">
                        <img src="assets/img/logo-2.png" alt="logo">
                    </a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#appside_main_menu" 
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="appside_main_menu">
                <ul class="navbar-nav">
                    <li class="current-menu-item">
                        <a href="#">Home</a></li>
                    <li><a href="#about">About</a></li>
                     <li><a href="#team">Team</a></li>
                    <li><a href="#drive">Drive with us</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </div>
            
        </div>
</nav>

    <!-- navbar area end -->

    <!-- header area start  -->
    <header class="header-area header-bg-9 style-09" id="home">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-6">
                    <div class="header-inner">
                        <h1 class="title wow fadeInDown">Revolutionising the RMC industry in one touch.
</h1>
                        <p>Download the MY RMC app</p>
                        <div class="btn-wrapper wow fadeInUp">
                            <a href="https://play.google.com/store/apps/details?id=com.myrmc.vendor.app" class="boxed-btn-02 reverse-color"><i class="flaticon-android-logo"></i> Play Store</a>
                            <a href="download/index.html" class="boxed-btn-02 blank"><i class="flaticon-apple-1"></i> App Store</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="right-img">
                        <div class="img-wrapper">
                            <img src="assets/img/header-right.png" alt="header right image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header area end  -->

    <!-- counterup area start  -->
    <!-- <section class="counterup-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="section-title">
                        <h2 class="title">Quickest way to book a transit mixer without any hassle
                            even</h2>

                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="single-counterup-style-02">
                                <div class="count-text">
                                   <div class="count-wrap"><span class="count-num">7</span>K+</div>
                                </div>
                                <div class="content">
                                    <div class="title">Total Downloads</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="single-counterup-style-02">
                                <div class="count-text">
                                    <div class="count-wrap"><span class="count-num">3.4</span>K</div>
                                </div>
                                <div class="content">
                                    <div class="title">Happy Users</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="single-counterup-style-02">
                                <div class="count-text">
                                    <div class="count-wrap"><span class="count-num">1.3</span>K</div>
                                </div>
                                <div class="content">
                                    <div class="title">Good Reviews</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- counterup area end -->

    <div class="about-app-area padding-top-120" id="about" style="background-image:url(assets/img/bg/about-app-area-bg.png);">
        <div class="container">
            <div class="row">
              
            </div>
            <div class="row love-this-app-area padding-top-120">
                <div class="col-lg-6">
                    <div class="img-wrapper">
                        <img src="assets/img/my-vehicle.png" alt="about mobile apps">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right-side-content">
                        <div class="section-title left-aligned">
                            <span class="subtitle">What we offer?</span>
                            <h2 class="title">Hassle-Free Transit mixer rental</h2>
                            <p>Book a transit mixer online. Whenever you need, wherever you need, how many ever
you need.</p>
                        </div>
                    </div>
                </div>
                <div style="padding-top: 2rem"class="col-lg-12">
                  <div class="full-width-video-area margin-top-minus-290">
                        <div class="img-full-width-video">
                            <img src="assets/img/full-width-video-area.jpg" alt="full width video area">
                            
                            </div>
                        </div>
                  </div>
                </div>
            </div>
            <div style="padding-left: .5rem;" class="row icon-box-wrapper-03 padding-top-120 padding-bottom-80 ">
                <div class="col-lg-6">
                    <div class="single-icon-box-03 margin-bottom-40 theme-04">
                        <div class="icon">
                            <i class="fas fa-rupee-sign"></i>
                        </div>
                        <div class="content">
                            <h4 class="title">Transparent Pricing</h4>
                            <p>Enjoy the most affordable rates in town with our transparent pricing.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-icon-box-03 margin-bottom-40 theme-04">
                        <div class="icon">
                           <i class="fas fa-search-location"></i>
                        </div>
                        <div class="content">
                            <h4 class="title">Realtime Tracking</h4>
                            <p>Track the transit mixer from anywhere in the world in one touch.
.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-icon-box-03 margin-bottom-40 theme-04">
                        <div class="icon">
                            <i class="fas fa-user-shield"></i>
                        </div>
                        <div class="content">
                            <h4 class="title">Safe and Reliable Trucks</h4>
                            <p>Superior safety ensured with our team of verified & trained partners.
.</p>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-icon-box-03 margin-bottom-40 theme-04">
                        <div class="icon">
                           <i class="fas fa-weight"></i>
                        </div>
                        <div class="content">
                            <h4 class="title">Accurate Diesel consumption</h4>
                            <p>With the help of GPS our innovative AI calculates the diesel consumption adding it to
the total cost of the trip.</p>
                        </div>

                    </div>

                </div>
                    <div class="col-lg-6">
                    <div class="single-icon-box-03 margin-bottom-40 theme-04">
                        <div class="icon">
                         <i class="fab fa-superpowers"></i>
                        </div>
                        <div class="content">
                            <h4 class="title">Determine driver efficiency effectively</h4>
                            <p>Track your drivers every trip helping you to calculate and plan ahead..</p>
                        </div>

                    </div>

                </div> <div class="col-lg-6">
                    <div class="single-icon-box-03 margin-bottom-40 theme-04">
                        <div class="icon">
                       <i class="fab fa-amazon-pay"></i>
                        </div>
                        <div class="content">
                            <h4 class="title">Immediate payment </h4>
                            <p>Gone are the times when you have to run behind your clients for payments. Get immediate payment from MyRMC app for trip.</p>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

<!-- full width feature area start -->
<!-- <section class="full-width-feature-area-03 gray-bg padding-top-120 padding-bottom-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="full-width-feature-style-03">
                    <div class="row reorder-xs">
                        <div class="col-lg-5">
                            <div class="content-area">
                                <div class="section-title left-aligned">
                                    <span class="subtitle">Communicate faster than ever</span>
                                    <h3 class="title">Reply to your customers in time</h3>
                                    <p>Messenger communication consectetur adipiscing elit, sed do eiusmod tempor  labore. it now takes seconds to communicate with your customers.</p>
                                    <div class="btn-wrapper">
                                        <a href="#" class="boxed-btn-02">Download Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="img-wrapper">
                                <img src="assets/img/book1.png" alt="full width feature">
                            </div>
                        </div>
                    </div>
                </div>
                <! <div class="full-width-feature-style-03 bg-cover padding-top-120">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="img-wrapper">
                                <img src="assets/img/sub.png" alt="full width feature">
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="content-area">
                                <div class="section-title left-aligned">
                                    <span class="subtitle">Unique Features that never seen </span>
                                    <h3 class="title">Discover even more possibilities.</h3>
                                    <p>Messenger communication consectetur adipiscing elit, sed do eiusmod tempor  labore. it now takes seconds to communicate with your customers.</p>
                                    <div class="btn-wrapper">
                                        <a href="#" class="boxed-btn-02">Download Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

    <!--         </div>
        </div>
    </div>
</section -->
<section>
    

    <div>
        
             <div class="section-title">
                    
                    <h3 class="title extra">Learn with Us!</h3>
                    <p>Looking and tracking your delivery is super easy now!.</p>
                </div>
                <div>
                    
                    <a id="play-video" class="video-play-button" href="#">
  <span></span>
</a>
                </div>



<div id="video-overlay" class="video-overlay">
  <a class="video-overlay-close">&times;</a>
</div>


  
                <img src="assets/img/svg.png">

             
        </div>
        
                  
                            

                         
               
                
        



   
</section>




 

<!-- full width feature area end -->

<!-- screenshort area start  -->
<section class="screenshort-area-02 padding-top-110 padding-bottom-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="section-title"><!-- section title -->
                    <span class="subtitle">Experience the amazing </span>
                    <h3 class="title extra">Easy to use interface</h3>
                 <!--    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor  tempor incididunt ut labore dolore magna.</p> -->
                </div><!-- / /. section title -->
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="screeshort-carousel-wrap-02">
                   <!--  <div class="mobile-cover">
                        <img src="assets/img/mobile.png" alt=""> -->
                    </div>
                    <div class="screenshort-carousel-02">
                        <div class="single-screenshort-item">
                            <img src="assets/img/screenshort/01.jpg" alt="screenhshort image">
                        </div>
                        <div class="single-screenshort-item">
                            <img src="assets/img/screenshort/02.jpg" alt="screenhshort image">
                        </div>
                        <div class="single-screenshort-item">
                            <img src="assets/img/screenshort/03.jpg" alt="screenhshort image">
                        </div>
                        <div class="single-screenshort-item">
                            <img src="assets/img/screenshort/04.jpg" alt="screenhshort image">
                        </div>
                         <div class="single-screenshort-item">
                            <img src="assets/img/screenshort/05.jpg" alt="screenhshort image">
                        </div>
                        <div class="single-screenshort-item">
                            <img src="assets/img/screenshort/06.jpg" alt="screenhshort image">
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- screenshort area end -->
 <!-- testimonial-two-area start  -->
 <!-- <div class="testimonial-area-two padding-top-120 gray-bg padding-bottom-120">
     <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="section-title"><
                    <span class="subtitle">Testimonial</span>
                    <h3 class="title extra">What People Say</h3>
                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor  tempor incididunt ut labore dolore magna.</p>
                </div>
            </div>
        </div>
     </div>
     <div class="testimonial-carousel-02">
        <div class="single-testimonial-item-02">
             <div class="img-wrapper">
                 <div class="bg-image" style="background-image: url(assets/img/testimonial/style-02/01.jpg)"></div>
             </div>
             <div class="content-area">
                 <p>Unwilling departure education is be dashwoods or an. Use off agreeable law unwilling sir deficient curiosity instantly. </p>
                 <div class="author-meta">
                     <h4 class="title">Jonathon Doe</h4>
                     <span class="designation">Directer, art media</span>
                 </div>
             </div>
        </div>
        <div class="single-testimonial-item-02">
             <div class="img-wrapper">
                 <div class="bg-image" style="background-image: url(assets/img/testimonial/style-02/01.jpg)"></div>
             </div>
             <div class="content-area">
                <p>Unwilling departure education is be dashwoods or an. Use off agreeable law unwilling sir deficient curiosity instantly. </p>
                 <div class="author-meta">
                     <h4 class="title">Jonathon Doe</h4>
                     <span class="designation">Directer, art media</span>
                 </div>
             </div>
        </div>
        <div class="single-testimonial-item-02">
             <div class="img-wrapper">
                 <div class="bg-image" style="background-image: url(assets/img/testimonial/style-02/01.jpg)"></div>
             </div>
             <div class="content-area">
                <p>Unwilling departure education is be dashwoods or an. Use off agreeable law unwilling sir deficient curiosity instantly. </p>
                 <div class="author-meta">
                     <h4 class="title">Jonathon Doe</h4>
                     <span class="designation">Directer, art media</span>
                 </div>
             </div>
        </div>
        <div class="single-testimonial-item-02">
             <div class="img-wrapper">
                 <div class="bg-image" style="background-image: url(assets/img/testimonial/style-02/01.jpg)"></div>
             </div>
             <div class="content-area">
                <p>Unwilling departure education is be dashwoods or an. Use off agreeable law unwilling sir deficient curiosity instantly. </p>
                 <div class="author-meta">
                     <h4 class="title">Jonathon Doe</h4>
                     <span class="designation">Directer, art media</span>
                 </div>
             </div>
        </div>
        <div class="single-testimonial-item-02">
             <div class="img-wrapper">
                 <div class="bg-image" style="background-image: url(assets/img/testimonial/style-02/01.jpg)"></div>
             </div>
             <div class="content-area">
                <p>Unwilling departure education is be dashwoods or an. Use off agreeable law unwilling sir deficient curiosity instantly. </p>
                 <div class="author-meta">
                     <h4 class="title">Jonathon Doe</h4>
                     <span class="designation">Directer, art media</span>
                 </div>
             </div>
        </div>
     </div>
 </div> -->
 <!-- testimonial-two-area end  -->

<!-- price plan area start -->
<section hidden class="pricing-plan-area" id="pricing">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="section-title"><!-- section title -->
                    <span class="subtitle">Pricing plans</span>
                    <h3 class="title extra">Afforadble Pricing</h3>
                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor  tempor incididunt ut labore dolore magna.</p>
                </div><!-- //. section title -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-price-plan-02 wow zoomIn"><!-- single price plan one -->
                    <div class="price-header">
                        <h4 class="name">Primary Plan</h4>
                        <div class="price-wrap">
                            <span class="price">$250</span>
                            <span class="month">/Mo</span>
                        </div>
                    </div>
                    <div class="price-body">
                        <ul>
                            <li>5 Analyzer</li>
                            <li>3 Month Support</li>
                            <li>10 Sessions</li>
                            <li>No Risk Garrunty</li>
                        </ul>
                    </div>
                    <div class="price-footer">
                        <a href="#" class="boxed-btn">Get Started</a>
                    </div>
                </div><!-- //. single price plan one -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-price-plan-02 wow zoomIn featured"><!-- single price plan one -->
                    <div class="price-header">
                        <h4 class="name">Basic Plan</h4>
                        <div class="price-wrap">
                            <span class="price">$350</span>
                            <span class="month">/Mo</span>
                        </div>
                    </div>
                    <div class="price-body">
                        <ul>
                            <li>5 Analyzer</li>
                            <li>3 Month Support</li>
                            <li>10 Sessions</li>
                            <li>No Risk Garrunty</li>
                        </ul>
                    </div>
                    <div class="price-footer">
                        <a href="#" class="boxed-btn">Get Started</a>
                    </div>
                </div><!-- //. single price plan one -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-price-plan-02 wow zoomIn"><!-- single price plan one -->
                    <div class="price-header">
                        <h4 class="name">Advance Plan</h4>
                        <div class="price-wrap">
                            <span class="price">$150</span>
                            <span class="month">/Mo</span>
                        </div>
                    </div>
                    <div class="price-body">
                        <ul>
                            <li>5 Analyzer</li>
                            <li>3 Month Support</li>
                            <li>10 Sessions</li>
                            <li>No Risk Garrunty</li>
                        </ul>
                    </div>
                    <div class="price-footer">
                        <a href="#" class="boxed-btn">Get Started</a>
                    </div>
                </div><!-- //. single price plan one -->
            </div>
        </div>
    </div>
</section>
<!-- price plan area end -->
        
<!-- team member area start -->
<section class="team-member-area gray-bg padding-bottom-120" id="team">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="section-title"><!-- section title -->
                    
                    <h3 class="title">Meet Our Team</h3>
                    
                </div><!-- //. section title -->
            </div>
        </div>
        <div class="row justify-content-center">


            <div class="col-lg-7.5">
               
               <!--  <div class="team-carousel"> --><!-- team carousel -->
                    <div class="single-team-member "><!-- single team member -->
                        <div class="thumb">
                            <img src="assets/img/team-member/sam.jpg" alt="team member image">
                            <div class="hover">
                                <ul class="social-icon">
                                <!--     <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li> -->
                                </ul>
                            </div>
                        </div>
                        <div class="content">
                            <h2 class="title">Kratu <br>Viswanath  </h2>
                            <span class="post">Founder</span>
                            <div class="btn-wrapper wow fadeInUp">
                            <a href="founder.html" class="boxed-btn-02 reverse-color"></i> know more</a>
                        </div>
                        </div>
                    </div><!-- //. single team member -->
                    <div style="" class="single-team-member "><!-- single team member -->
                        <div class="thumb">
                            <img src="assets/img/team-member/sam.jpg" alt="team member image">
                            <div class="hover">
                                <ul class="social-icon">
                                 <!--    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li> -->
                                </ul>
                            </div>
                        </div>
                        <div class="content">
                            <h4 class="title">Puliadi Amarnath <br> Aswinkumaar</h4>
                            <span class="post">CFO</span>
                            <div  class="btn-wrapper wow fadeInUp">
                            <a href="cfo.html" class="boxed-btn-02 reverse-color"></i> know more</a>
                        </div>
                        </div>
                    </div><!-- //. single team member -->
                    <div class="single-team-member "><!-- single team member -->
                        <div class="thumb">
                            <img src="assets/img/team-member/sam.jpg" alt="team member image">
                            <div class="hover">
                                <ul class="social-icon">
                              <!--       <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li> -->
                                </ul>
                            </div>
                        </div>
                        <div class="content">
                            <h4 class="title">Elango S</h4>
                            <span class="post">Advisor</span>
                       
                        <div style="padding-top: 2rem;" class="btn-wrapper wow fadeInUp">
                            <a href="gs.html" class="boxed-btn-02 reverse-color"></i> know more</a>
                        </div>
                    </div><!-- //. single team member -->
                   <!-- <div class="single-team-member "> --> <!-- single team member -->
                      <!--    <div class="thumb">
                            <img src="assets/img/team-member/04.jpg" alt="team member image">
                            <div class="hover">
                                <ul class="social-icon">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="content">
                            <h4 class="title">Lara Croft</h4>
                            <span class="post">CEO, Appside</span>
                        </div> -->
                   <!--  </div> --> <!-- //. single team member -->
                    <!--  <div class="single-team-member "> --><!-- single team member -->
                       <!--  <div class="thumb">
                            <img src="assets/img/team-member/05.jpg" alt="team member image">
                            <div class="hover">
                                <ul class="social-icon">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="content">
                            <h4 class="title">Eiusmoy Smith</h4>
                            <span class="post">Developer</span>
                            <div style="padding-top: 2rem;" class="btn-wrapper wow fadeInUp">
                            <a href="gs.html" class="boxed-btn-02 reverse-color"></i> know more</a>
                        </div> -->

                        </div>
                    </div> <!-- //. single team member -->
                </div><!-- //. team carousel -->
            </div>
        </div>
    </div>
</section>
<!-- team member area end -->
<section style="padding-top: 100px;">
    <div class="testimonial-area-two padding-top-120 gray-bg padding-bottom-120"  id="drive">
     <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="section-title"><!-- section title -->
                   
                    <h3 class="title extra"> Drive with us</h3>

                    
                </div><!-- / /. section title -->
            </div>
        </div>
     </div>
<div class="container">
    


    <!--  -->
  <div class="col-lg-12">
                    <ul class="feature-list style-03">
                        <div class="col-lg-4">
                        <li class="single-feature-list" >
                            <div class="icon icon-bg-1">
                             <i class="fas fa-money-bill-wave"></i>
                            </div>
                            <div class="content">
                                <h4 class="title">High earning opportunity</h4>
                                <p>Increase your transit mixers booking and reduce idle <br>time by getting bookings <br>from new clients. </p>
                            </div>
                        </li>
                        </div>
                        <div class="col-lg-4">
                        <li class="single-feature-list">
                            <div class="icon icon-bg-2">
                            <i class="fas fa-object-ungroup"></i>
                            </div>
                            <div class="content">
                                <h4 class="title">Easy 2 steps registration</h4>
                                <p>Register in two simple steps and add all your details about your business. Sit back and relax and watch your business grow though MyRMC. </p>
                            </div>
                        </li>
                        </div>
                        <div class="col-lg-4">
                        <li class="single-feature-list">
                            <div class="icon icon-bg-3">
                           <i class="fas fa-business-time"></i>
                            </div>
                            <div class="content">
                                <h4 class="title">On-time payment</h4>
                                <p>Forget about following up on payments. MyRMC will take care of that for you. Receive on-time payments for work without any hassle.  </p>
                            </div>
                        </li>
                        </div>
                        <!-- <li class="single-feature-list">
                            <div class="icon icon-bg-4">
                                <i class="flaticon-picture"></i>
                            </div>
                            <div class="content">
                                <h4 class="title"><a href="#">Access Drive</a></h4>
                                <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor </p>
                            </div>
                        </li> -->
                    </ul>
                </div>
     
</div>


         



</div> 


</section>

   


<!-- contact form area start  -->
<div class="contact-form-area-02 contact-bg padding-bottom-120 padding-top-120" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                    <div class="contact-area-wrapper" id="contact"><!-- contact area wrapper -->
                       
                        <h3 class="title">I Would Like To Attach</h3>
                        <!-- <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor  tempor incididunt ut labore dolore magna.</p> -->
                        <form id="contact_form_submit class="contact-form sec-margin" action="#" method="post">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="uname" name="name"placeholder="Your Name">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Your Email">
                                    </div>
                                </div>
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="company name" class="form-control" id="company name" name="company_name" placeholder="Company Name">
                                    </div>
                                </div>
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="mobile number" class="form-control" id="mobile number" name="mobile_number" placeholder="Your Mobile Number">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group textarea">
                                        <textarea name="message" id="message" class="form-control" cols="30" rows="10" placeholder="Message"></textarea>
                                    </div>
                                    <input class="submit-btn-02" name="submit" type="submit" value="submit">
                                </div>
                            </div>
                        </form>
                    </div><!-- //. contact area wrapper -->
                </div>
            <div class="col-lg-6">
                <div class="img-wrapper">
                    <img style="max-width:80%"src="assets/img/footer-right.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- contact form area end -->


<div class="call-to-action-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="call-to-action-inner-style-02">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <h3 class="title">App is available for free on Google Play & App Store</h3>
                            <div class="btn-wrapper wow fadeInUp">
                                <a href=https://play.google.com/store/apps/details?id=com.myrmc.vendor.app" class="boxed-btn-02 reverse-color"><i class="flaticon-android-logo"></i> Play Store</a>
                                <a href="download/index.html" class="boxed-btn-02 blank"><i class="flaticon-apple-1"></i> App Store</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- footer area start -->
<footer  class="footer-area style-02 bg-blue">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="footer-widget about_widget">
                        <a href="index.html" class="footer-logo"><img src="assets/img/logo-3.png" alt=""></a>
                        <p> MY RMC app is an one stop solution for booking and hiring Transit mixer, Pump and Gang and Boom Plazer. Download the app now and enjoy the benefits. </p>
                       <!--  <ul class="social-icon">
                            <li><a href="https://www.facebook.com/MyRmc-100231255537075"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        </ul> -->
                    </div>
                </div>
                <div class="col-lg-2 col-md-2">
                    <div class="footer-widget nav_menus_widget">
                        <h4 class="widget-title">Important links</h4>
                        <ul>

                       <li><a href="terms.html"> Terms of Service</p> </a></li>
                          <li><a href="privacy.html">Privacy Policy </a></li>
                         <li><a href="refund.html">Refund Policy </a></li>



                        </ul>
                        
                    </div>
                </div>
             <!--    <div hidden class="col-lg-4 col-md-6">
                    <div class="footer-widget nav_menus_widget">
                        <h4 class="widget-title">Need Help?</h4>
                        <ul>
                            <li><a href="#"><i class="fas fa-chevron-right"></i> Faqs</a></li>
                            <li><a href="#"><i class="fas fa-chevron-right"></i> Privacy</a></li>
                            <li><a href="#"><i class="fas fa-chevron-right"></i> Policy</a></li>
                            <li><a href="#"><i class="fas fa-chevron-right"></i> Support</a></li>
                            <li><a href="#"><i class="fas fa-chevron-right"></i> Temrs</a></li>
                        </ul>
                    </div>
                </div> -->
                <div class="col-lg-4 col-md-4">
                    <div class="footer-widget nav_menus_widget">
                        <h4 class="widget-title">Reach us</h4>
                        <ul>
                            <p style="color:rgba(255, 255, 255, 0.8);"><i class="fas fa-building"></i> G-115, PHASE-II, SPENCER PLAZA, NO. 768, Anna Salai, Chennai, Tamil Nadu, 600002</p>
                            <p style="color:rgba(255, 255, 255, 0.8);"><i class="fa fa-envelope"></i> support@myrmc.com </p>
                            <p style="color:rgba(255, 255, 255, 0.8);"><i class="fas fa-phone-square"></i> +91 9884305275</p> 
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-area"><!-- copyright area -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright-inner"><!-- copyright inner wrapper -->
                        <div class="left-content-area"><!-- left content area -->
                            &copy; Copyrights 2021 MY RMC All rights reserved.
                        </div><!-- //. left content aera -->
                        <div class="right-content-area"><!-- right content area -->
                             Designed by  <li><a href="http://creatah.com/"><strong>CREATAH</strong>  </a></li>
                        </div><!-- //. right content area -->
                    </div><!-- //.copyright inner wrapper -->
                </div>
            </div>
        </div>
    </div><!-- //. copyright area -->


</footer>
<!-- footer area end -->

<!-- preloader area start -->
<!-- <div class="preloader-wrapper" id="preloader">
    <div class="preloader" >
        <div class="sk-circle">
            <div class="sk-circle1 sk-child"></div>
            <div class="sk-circle2 sk-child"></div>
            <div class="sk-circle3 sk-child"></div>
            <div class="sk-circle4 sk-child"></div>
            <div class="sk-circle5 sk-child"></div>
            <div class="sk-circle6 sk-child"></div>
            <div class="sk-circle7 sk-child"></div>
            <div class="sk-circle8 sk-child"></div>
            <div class="sk-circle9 sk-child"></div>
            <div class="sk-circle10 sk-child"></div>
            <div class="sk-circle11 sk-child"></div>
            <div class="sk-circle12 sk-child"></div>
        </div>
    </div>
</div> -->
</style>
<!-- 
 <a href="https://wa.me/919944308008" target="_blank" class="float">
    <img src="assets/img/whatsapp.png" width="65px">
</a>

<style>
.float {
    position: fixed;
    width: 60px;
    height: 60px;
    bottom: 23px;
    left: 30px; 
   /* right: 18px;*/
    z-index: 100;
    color: #FFF;
    border-radius: 20px;
    text-align: center;
    box-shadow: 2px 2px 3px #9990;
}

</style> -->
 

  <!-- back to top area start -->
  <div class="back-to-top">
        <i class="fas fa-angle-up"></i>
  </div>
  <!-- back to top area end -->

    <!-- jquery -->
    <script src="assets/js/jquery.js"></script>
    <!-- popper -->
    <script src="assets/js/popper.min.js"></script>
    <!-- bootstrap -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- owl carousel -->
    <script src="assets/js/owl.carousel.min.js"></script>
    <!-- magnific popup -->
    <script src="assets/js/jquery.magnific-popup.js"></script>
    <!-- contact js-->
    <script src="assets/js/contact.js"></script>
    <!-- wow js-->
    <script src="assets/js/wow.min.js"></script>
    <!-- way points js-->
    <script src="assets/js/waypoints.min.js"></script>
    <!-- counterup js-->
    <script src="assets/js/jquery.counterup.min.js"></script>
    <!-- main -->
    <script src="assets/js/main.js"></script>
    <script type="text/javascript">
    if(window.history.replaceState){
      window.history.replaceState(null, null, window.location.href);
    }
    </script>
<script src="assets/js/video.js"></script>
</body>


</html>